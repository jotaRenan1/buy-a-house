/* eslint-disable @typescript-eslint/naming-convention */
import { GlobalStyle } from './shared/global-styles';
import Scaffold from './components/scaffold';
import { ThemeProvider } from 'styled-components';
import theme from './shared/theme';

export function App(): JSX.Element {
  return (
    <>
      <GlobalStyle />
      <ThemeProvider theme={theme}>
        <Scaffold />
      </ThemeProvider>
    </>
  );
}
