import { shallow } from 'enzyme';

import { App } from './App';

describe('Scaffold', () => {
  it('displays the correct to action', () => {
    const component = shallow(<App />);

    expect(component.exists()).toBeTruthy();
  });
});
