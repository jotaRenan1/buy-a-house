const offsetDate =
  (offsetInMonths: number) =>
  (date: Readonly<Date>): Date =>
    new Date(
      date.getFullYear(),
      date.getMonth() + offsetInMonths,
      date.getDate()
    );
const getDateIncreasedByOneMonth = offsetDate(+1);
const getDateDecreasedByOneMonth = offsetDate(-1);

const offsetDateInMonths = (
  date: Readonly<Date>,
  offsetInMonths: number
): Date =>
  new Date(
    date.getFullYear(),
    date.getMonth() + offsetInMonths,
    date.getDate()
  );

// Taken from https://stackoverflow.com/a/29273131/7180873
function parseLocaleNumber(stringNumber: string, locale: string): number {
  const thousandSeparator = Intl.NumberFormat(locale)
    .format(11111)
    .replace(/\p{Number}/gu, '');
  const decimalSeparator = Intl.NumberFormat(locale)
    .format(1.1)
    .replace(/\p{Number}/gu, '');

  return parseFloat(
    stringNumber
      .replace(new RegExp('\\' + thousandSeparator, 'g'), '')
      .replace(new RegExp('\\' + decimalSeparator), '.')
  );
}

export {
  parseLocaleNumber,
  offsetDateInMonths,
  offsetDate,
  getDateIncreasedByOneMonth,
  getDateDecreasedByOneMonth,
};
