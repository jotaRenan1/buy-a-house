import { createGlobalStyle } from 'styled-components';
import theme from './theme';

const globalStyle = createGlobalStyle`
  body {
    margin: 0;
    padding: 0;
    
    font-family: ${theme.fonts.primary};

    & * {
      outline-color: ${theme.brandColorSecondary};
  }
  }

  * {
      margin: 0;
      padding: 0;
      box-sizing: border-box;
  }


`;

export { globalStyle as GlobalStyle };
