const theme = {
  brandColorPrimary: '#1b31a8',
  brandColorSecondary: '#0079ff',
  neutralWhite: '#fff',
  blueGray10: '#f4f8fa',
  blueGray50: '#e9eef2',
  blueGray100: '#cbd5dc',
  blueGray300: '#8a9ca9',
  blueGray400: '#708797',
  blueGray600: '#4d6475',
  blueGray900: '#1e2a32',
  fonts: {
    primary: "'Work Sans', sans-serif",
    secondary: "'Rubik', sans-serif",
  },
} as const;

export type ApplicationTheme = typeof theme;

export default theme;
