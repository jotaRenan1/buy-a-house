/* eslint-disable @typescript-eslint/naming-convention */
import { css } from 'styled-components';
import theme from './theme';

const inputsBorder = css`
  border-radius: 4px;
  border: 1px solid ${theme.blueGray50};
`;

const buttonStylesReset = css`
  background: transparent;
  box-shadow: 0px 0px 0px transparent;
  border: 0px;
  text-shadow: 0px 0px 0px transparent;
`;

export { inputsBorder, buttonStylesReset };
