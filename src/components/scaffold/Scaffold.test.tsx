import renderer from 'react-test-renderer';
import { ThemeProvider } from 'styled-components';
import Scaffold from '.';
import { render, screen } from '../../shared/test-utils';
import theme from '../../shared/theme';

// eslint-disable-next-line react/display-name, @typescript-eslint/no-explicit-any
jest.mock('react-currency-input', () => (props: any) => (
  <input type="text" {...{ ...props }} />
));

describe('Scaffold', () => {
  describe('screen overview', () => {
    it('displays the correct to action', () => {
      render(<Scaffold />);
      expect(screen.getByTestId('callToAction')).toHaveTextContent(
        "Let's plan your saving goal."
      );
    });

    it('renders correctly', () => {
      const tree = renderer
        .create(
          <ThemeProvider theme={theme}>
            <Scaffold />
          </ThemeProvider>
        )
        .toJSON();
      expect(tree).toMatchSnapshot();
    });
  });
});
