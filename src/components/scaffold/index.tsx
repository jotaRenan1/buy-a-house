import React from 'react';
import BuyAHouseForm from '../buy-a-house-form';
import { ReactComponent as CompanyLogo } from '../../assets/icons/origin-logo.svg';
import { ApplicationContent, CallToAction, Container, Header } from './styles';

export default function Scaffold(): JSX.Element {
  function handleSubmit(e: React.FormEvent<HTMLFormElement>) {
    e.preventDefault();
    const formData = new FormData(e.currentTarget);
    const formProps = Object.fromEntries(formData);
    console.table(formProps);
    // This is where we would do fun stuff.
  }
  return (
    <Container>
      <Header>
        <CompanyLogo className="logo" />
      </Header>
      <ApplicationContent>
        <CallToAction data-testid="callToAction">
          Let&apos;s plan your <strong>saving goal</strong>.
        </CallToAction>
        <BuyAHouseForm onSubmit={handleSubmit}></BuyAHouseForm>
      </ApplicationContent>
    </Container>
  );
}
