import styled from 'styled-components';

const header = styled('header')`
  padding: 1.5rem 3.5rem;
  .logo {
    height: 2rem;
  }
  @media only screen and (max-width: 378px) {
    padding: 1rem;
    .logo {
      height: 1.5rem;
    }
  }
`;

const container = styled('div')`
  display: flex;
  flex-direction: column;
  min-height: 100vh;
  margin: 0;
  padding: 0;
`;

const callToAction = styled('span')`
  color: ${(props) => props.theme.brandColorPrimary};
  font-size: 1.25rem;
  margin: 3rem 0 2rem;

  @media only screen and (max-width: 378px) {
    margin: 2rem 0 1.5rem;
  }
`;

const applicationContent = styled('main')`
  background-color: ${(props) => props.theme.blueGray10};
  flex: 1;
  display: flex;
  flex-direction: column;
  align-items: center;
`;

export {
  applicationContent as ApplicationContent,
  callToAction as CallToAction,
  container as Container,
  header as Header,
};
