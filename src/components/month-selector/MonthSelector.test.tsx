import { fireEvent, render, screen } from '../../shared/test-utils';
import MonthSelector from './index';

describe('MonthSelector component', () => {
  const oldDate = new Date(1999, 8, 29);
  const recentDate = new Date(2000, 4, 6);

  beforeAll(() => jest.resetAllMocks());

  it('should invoke onDecrease when pressing the left arrow key', () => {
    const onDecreaseHandlerMock = jest.fn();
    render(
      <MonthSelector
        onIncrease={jest.fn()}
        onDecrease={onDecreaseHandlerMock}
        date={recentDate}
        min={oldDate}
      />
    );

    fireEvent.keyDown(screen.getByTestId('monthSelectorContainer'), {
      key: 'ArrowLeft',
      code: 'ArrowLeft',
    });
    expect(onDecreaseHandlerMock).toBeCalled();
  });

  it('should invoke onDecrease when clicking the Previous button', () => {
    const onDecreaseHandlerMock = jest.fn();
    render(
      <MonthSelector
        onIncrease={jest.fn()}
        onDecrease={onDecreaseHandlerMock}
        date={oldDate}
      />
    );

    fireEvent.click(screen.getByTestId('decreaseMonthNavigator'));
    expect(onDecreaseHandlerMock).toBeCalled();
  });

  it('should invoke onIncrease when pressing the right arrow key', () => {
    const onIncreaseHandlerMock = jest.fn();
    render(
      <MonthSelector
        onIncrease={onIncreaseHandlerMock}
        onDecrease={jest.fn()}
        date={oldDate}
      />
    );

    fireEvent.keyDown(screen.getByTestId('monthSelectorContainer'), {
      key: 'ArrowRight',
      code: 'ArrowRight',
    });
    expect(onIncreaseHandlerMock).toBeCalled();
  });

  it('should invoke onIncrease when clicking the Next button', () => {
    const onIncreaseHandlerMock = jest.fn();
    render(
      <MonthSelector
        onIncrease={onIncreaseHandlerMock}
        onDecrease={jest.fn()}
        date={oldDate}
      />
    );

    fireEvent.click(screen.getByTestId('increaseMonthNavigator'));
    expect(onIncreaseHandlerMock).toBeCalled();
  });

  it('should decrease the month when clicking the Previous button', () => {
    render(
      <MonthSelector
        onIncrease={jest.fn()}
        onDecrease={jest.fn()}
        date={recentDate}
      />
    );

    fireEvent.click(screen.getByTestId('decreaseMonthNavigator'));

    expect(screen.getByTestId('formattedMonthDisplay')).toHaveTextContent(
      'April2000'
    );

    fireEvent.click(screen.getByTestId('decreaseMonthNavigator'));
    fireEvent.click(screen.getByTestId('decreaseMonthNavigator'));
    fireEvent.click(screen.getByTestId('decreaseMonthNavigator'));
    fireEvent.click(screen.getByTestId('decreaseMonthNavigator'));

    expect(screen.getByTestId('formattedMonthDisplay')).toHaveTextContent(
      'December1999'
    );
  });

  it('should increase the month when clicking the Next button', () => {
    const expectedValueAfterOneClick = 'October1999';
    const expectedValueAfterFourClicks = 'January2000';
    render(
      <MonthSelector
        onIncrease={jest.fn()}
        onDecrease={jest.fn()}
        date={oldDate}
      />
    );

    fireEvent.click(screen.getByTestId('increaseMonthNavigator'));

    expect(screen.getByTestId('formattedMonthDisplay')).toHaveTextContent(
      expectedValueAfterOneClick
    );

    fireEvent.click(screen.getByTestId('increaseMonthNavigator'));
    fireEvent.click(screen.getByTestId('increaseMonthNavigator'));
    fireEvent.click(screen.getByTestId('increaseMonthNavigator'));

    expect(screen.getByTestId('formattedMonthDisplay')).toHaveTextContent(
      expectedValueAfterFourClicks
    );
  });

  it('should contain the proper ARIA attributes', () => {
    render(
      <MonthSelector
        onIncrease={jest.fn()}
        onDecrease={jest.fn()}
        date={new Date()}
      />
    );

    expect(
      screen.getByTestId('formattedMonthDisplay').getAttribute('aria-live')
    ).toBe('polite');
    expect(
      screen.getByTestId('formattedMonthDisplay').hasAttribute('aria-atomic')
    ).toBe(true);
    expect(
      screen.getByTestId('monthSelectorContainer').getAttribute('aria-label')
    ).toBe(
      'Reach goal by. Choose a month. Navigate by using the left and right arrow keys.'
    );
  });

  it('should display the date prop formatted correctly', () => {
    render(
      <MonthSelector
        onIncrease={jest.fn()}
        onDecrease={jest.fn()}
        date={oldDate}
      />
    );

    expect(screen.getByTestId('formattedMonthDisplay')).toHaveTextContent(
      'September1999'
    );
  });

  describe('No min prop specified', () => {
    it('should invoke onDecrease when pressing the left arrow key ', () => {
      const onDecreaseHandlerMock = jest.fn();
      render(
        <MonthSelector
          onIncrease={jest.fn()}
          onDecrease={onDecreaseHandlerMock}
          date={oldDate}
        />
      );

      fireEvent.keyDown(screen.getByTestId('monthSelectorContainer'), {
        key: 'ArrowLeft',
        code: 'ArrowLeft',
      });
      expect(onDecreaseHandlerMock).toBeCalled();
    });
  });

  describe('Constraint: min = date', () => {
    it('should disable decrease button', () => {
      render(
        <MonthSelector
          onIncrease={jest.fn()}
          onDecrease={jest.fn()}
          date={oldDate}
          min={oldDate}
        />
      );

      expect(screen.getByTestId('decreaseMonthNavigator')).toBeDisabled();
    });

    it('should NOT invoke onDecrease when pressing the left arrow key', () => {
      const onDecreaseHandlerMock = jest.fn();
      render(
        <MonthSelector
          onIncrease={jest.fn()}
          onDecrease={onDecreaseHandlerMock}
          date={oldDate}
          min={oldDate}
        />
      );

      fireEvent.keyDown(screen.getByTestId('monthSelectorContainer'), {
        key: 'ArrowLeft',
        code: 'ArrowLeft',
      });
      expect(onDecreaseHandlerMock).not.toBeCalled();
    });

    it('month should remain the same after pressing the left arrow key', () => {
      render(
        <MonthSelector
          onIncrease={jest.fn()}
          onDecrease={jest.fn()}
          date={oldDate}
          min={oldDate}
        />
      );

      expect(screen.getByTestId('formattedMonthDisplay')).toHaveTextContent(
        'September1999'
      );
      fireEvent.keyDown(screen.getByTestId('monthSelectorContainer'), {
        key: 'ArrowLeft',
        code: 'ArrowLeft',
      });

      expect(screen.getByTestId('formattedMonthDisplay')).toHaveTextContent(
        'September1999'
      );
    });
  });

  describe('Constraint: date < min', () => {
    it('should disable decrease button', () => {
      render(
        <MonthSelector
          onIncrease={jest.fn()}
          onDecrease={jest.fn()}
          date={oldDate}
          min={recentDate}
        />
      );

      expect(screen.getByTestId('decreaseMonthNavigator')).toBeDisabled();
    });

    it('should NOT invoke onDecrease when pressing the left arrow key', () => {
      const onDecreaseHandlerMock = jest.fn();
      render(
        <MonthSelector
          onIncrease={jest.fn()}
          onDecrease={onDecreaseHandlerMock}
          date={oldDate}
          min={recentDate}
        />
      );

      fireEvent.keyDown(screen.getByTestId('monthSelectorContainer'), {
        key: 'ArrowLeft',
        code: 'ArrowLeft',
      });
      expect(onDecreaseHandlerMock).not.toBeCalled();
    });

    it('month should remain the same after pressing the left arrow key', () => {
      render(
        <MonthSelector
          onIncrease={jest.fn()}
          onDecrease={jest.fn()}
          date={oldDate}
          min={recentDate}
        />
      );

      expect(screen.getByTestId('formattedMonthDisplay')).toHaveTextContent(
        'September1999'
      );
      fireEvent.keyDown(screen.getByTestId('monthSelectorContainer'), {
        key: 'ArrowLeft',
        code: 'ArrowLeft',
      });

      expect(screen.getByTestId('formattedMonthDisplay')).toHaveTextContent(
        'September1999'
      );
    });
  });
});
