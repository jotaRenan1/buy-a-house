import React, { useEffect, useState } from 'react';
import { ReactComponent as LeftChevron } from '../../assets/icons/chevron-left.svg';
import { ReactComponent as RightChevron } from '../../assets/icons/chevron-right.svg';
import {
  getDateDecreasedByOneMonth,
  getDateIncreasedByOneMonth,
} from '../../shared/utils';
import { Container } from './styles';

enum KeyCodes {
  leftArrow = 'ArrowLeft',
  rightArrow = 'ArrowRight',
}

type MonthSelectorProps = {
  onIncrease: () => void;
  onDecrease: () => void;
  onChange?: (date: Readonly<Date>) => void;
  date: Readonly<Date>;
  min?: Readonly<Date>;
  className?: string;
};

function MonthSelector(props: MonthSelectorProps): JSX.Element {
  const {
    onChange,
    onDecrease: externalOnDecrease,
    onIncrease: externalOnIncrease,
    date,
    min,
    className,
  } = props;
  const [internalValue, setInternalValue] = useState<Readonly<Date>>(date);

  useEffect(() => {
    if (onChange) {
      onChange(internalValue);
    }
  }, [internalValue, onChange]);

  function onIncrease() {
    setInternalValue(getDateIncreasedByOneMonth(internalValue));
    externalOnIncrease();
  }

  function onDecrease() {
    setInternalValue(getDateDecreasedByOneMonth(internalValue));
    externalOnDecrease();
  }

  return (
    <Container
      className={className}
      aria-label="Reach goal by. Choose a month. Navigate by using the left and right arrow keys."
      tabIndex={0}
      data-testid="monthSelectorContainer"
      onKeyDown={(e) => {
        const { code } = e;

        if (code === KeyCodes.leftArrow && !shouldDecreaseNavBeDisabled()) {
          onDecrease();
        } else if (code === KeyCodes.rightArrow) {
          onIncrease();
        }
      }}
    >
      <button
        className="navigator"
        type="button"
        tabIndex={-1}
        onClick={onDecrease}
        disabled={min !== undefined ? shouldDecreaseNavBeDisabled() : false}
        data-testid="decreaseMonthNavigator"
      >
        <LeftChevron />
      </button>
      <div
        className="display"
        aria-live="polite"
        data-testid="formattedMonthDisplay"
        aria-atomic
      >
        <span className="month">
          <b>
            {Intl.DateTimeFormat('en-US', {
              month: 'long',
            }).format(internalValue as Date)}
          </b>
        </span>
        <span className="year">{internalValue.getFullYear()}</span>
      </div>
      <button
        className="navigator"
        type="button"
        tabIndex={-1}
        onClick={onIncrease}
        data-testid="increaseMonthNavigator"
      >
        <RightChevron />
      </button>
      <input
        type="text"
        name="reachDate"
        value={internalValue.toISOString()}
        data-testid="reachDateValue"
        readOnly
        hidden
      />
    </Container>
  );

  function shouldDecreaseNavBeDisabled(): boolean {
    if (min === undefined) return false;

    return (
      internalValue.getFullYear() < (min as Date).getFullYear() ||
      (internalValue.getFullYear() === (min as Date).getFullYear() &&
        internalValue.getMonth() <= (min as Date).getMonth())
    );
  }
}

export default MonthSelector;
