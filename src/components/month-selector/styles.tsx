import styled from 'styled-components';
import { buttonStylesReset, inputsBorder } from '../../shared/mixins';

const container = styled('div')`
  ${inputsBorder};
  display: flex;
  height: 100%;

  &:focus-within {
    outline-width: 2px;
    outline-style: solid;
  }

  button {
    outline: 0;
    ${buttonStylesReset}

    &:hover {
      ${buttonStylesReset}
      background: transparent;
      box-shadow: 0px 0px 0px transparent;
      border: 0px solid transparent;
      text-shadow: 0px 0px 0px transparent;
      cursor: pointer;

      &:disabled {
        cursor: not-allowed;
      }
    }
  }

  .navigator {
    padding-left: 0.5rem;
    padding-right: 0.5rem;

    &:disabled {
      opacity: 0.3;
    }
  }

  .display {
    margin: 0 auto;
    display: inline-flex;
    flex-direction: column;
    justify-content: center;
    gap: 0.25rem;
    min-width: 8ch;
    text-align: center;
    font-size: 1rem;

    .year {
      color: ${(props) => props.theme.blueGray400};
    }

    span {
      display: inline-block;
    }
  }
`;

export { container as Container };
