import { fireEvent, render, screen } from '../../shared/test-utils';
import BuyAHouseForm from '.';
import mockDate from 'mockdate';
import userEvent from '@testing-library/user-event';

describe('<BuyAHouseForm />', () => {
  const september1st1999SinceEpoch = 936154800000;

  beforeEach(() => mockDate.reset());

  it('should invoke onSubmit handler when form is submitted', () => {
    const handler = jest.fn();
    render(<BuyAHouseForm onSubmit={handler} />);
    fireEvent.submit(screen.getByTestId('buyAHouseFormContainer'));
    expect(handler).toBeCalledTimes(1);
  });

  it('value is initially set to zero', () => {
    render(<BuyAHouseForm onSubmit={jest.fn()} />);
    expect(screen.getByTestId('amountInput').getAttribute('value')).toBe('0');
  });

  it("month selector's initial value is 1 month ahead from the current month", () => {
    mockDate.set(september1st1999SinceEpoch);
    render(<BuyAHouseForm onSubmit={jest.fn()} />);
    expect(screen.getByTestId('formattedMonthDisplay')).toHaveTextContent(
      'October1999'
    );
  });

  it("month selector's decrease month button is initially disabled", () => {
    mockDate.set(september1st1999SinceEpoch);
    render(<BuyAHouseForm onSubmit={jest.fn()} />);
    expect(screen.getByTestId('decreaseMonthNavigator')).toBeDisabled();
  });

  describe('output section', () => {
    it('output changes as a result of Total Amount changes', () => {
      mockDate.set(september1st1999SinceEpoch);
      const [inputText, expected] = ['250', '$250'];
      render(<BuyAHouseForm onSubmit={jest.fn()} />);
      expect(screen.getByTestId('formattedTotalAmount')).toHaveTextContent(
        '$0'
      );
      fireEvent.change(screen.getByTestId('amountInput'), {
        target: { value: inputText },
      });
      expect(screen.getByTestId('formattedTotalAmount')).toHaveTextContent(
        expected
      );
    });

    it('output changes as a result of Month Increase', () => {
      mockDate.set(september1st1999SinceEpoch);
      render(<BuyAHouseForm onSubmit={jest.fn()} />);
      expect(screen.getByTestId('deadline')).toHaveTextContent('October 1999');
      fireEvent.click(screen.getByTestId('increaseMonthNavigator'));
      expect(screen.getByTestId('deadline')).toHaveTextContent('November 1999');
    });

    it('output changes as a result of Month Decrease', () => {
      mockDate.set(september1st1999SinceEpoch);
      render(<BuyAHouseForm onSubmit={jest.fn()} />);
      expect(screen.getByTestId('deadline')).toHaveTextContent('October 1999');
      fireEvent.click(screen.getByTestId('increaseMonthNavigator'));
      expect(screen.getByTestId('deadline')).toHaveTextContent('November 1999');
      fireEvent.click(screen.getByTestId('decreaseMonthNavigator'));
      expect(screen.getByTestId('deadline')).toHaveTextContent('October 1999');
    });

    it('output for 1 deposit is in the singular form', () => {
      render(<BuyAHouseForm onSubmit={jest.fn()} />);
      expect(screen.getByTestId('monthlyDeposits').textContent).toBe(
        '1 monthly deposit'
      );
    });

    it('output for over 1 deposit is in the plural form', () => {
      render(<BuyAHouseForm onSubmit={jest.fn()} />);
      fireEvent.click(screen.getByTestId('increaseMonthNavigator'));
      expect(screen.getByTestId('monthlyDeposits').textContent).toBe(
        '2 monthly deposits'
      );
    });
  });

  it('monthly goal is correctly calculated for integers', () => {
    mockDate.set(september1st1999SinceEpoch);
    render(<BuyAHouseForm onSubmit={jest.fn()} />);
    fireEvent.change(screen.getByTestId('amountInput'), {
      target: { value: '150' },
    });

    const monthlyAmount = screen.getByTestId('monthlyAmount');
    const increaseMonthNavigator = screen.getByTestId('increaseMonthNavigator');

    expect(monthlyAmount).toHaveTextContent('$150');
    fireEvent.click(increaseMonthNavigator);
    expect(monthlyAmount).toHaveTextContent('$75');
    fireEvent.click(increaseMonthNavigator);
    expect(monthlyAmount).toHaveTextContent('$50');
    fireEvent.click(screen.getByTestId('decreaseMonthNavigator'));
    expect(monthlyAmount).toHaveTextContent('$75');
  });

  describe('decimal formatting', () => {
    it.each([
      ['$9', '9'],
      ['$99', '99'],
      ['$999', '999'],
      ['$9,999', '9999'],
      ['$99,999', '99999'],
      ['$999,999', '999999'],
      ['$9,999,999', '9999999'],
    ])('monthly goal should display %s when goal is %s', (expected, array) => {
      mockDate.set(september1st1999SinceEpoch);
      render(<BuyAHouseForm onSubmit={jest.fn()} />);
      fireEvent.change(screen.getByTestId('amountInput'), {
        target: { value: array },
      });
      expect(screen.getByTestId('monthlyAmount').textContent).toEqual(expected);
    });
  });

  it('monthly goal is correctly calculated for fractions', () => {
    mockDate.set(september1st1999SinceEpoch);
    render(<BuyAHouseForm onSubmit={jest.fn()} />);
    fireEvent.change(screen.getByTestId('amountInput'), {
      target: { value: '100' },
    });

    const monthlyAmount = screen.getByTestId('monthlyAmount');
    const increaseMonthNavigator = screen.getByTestId('increaseMonthNavigator');

    expect(monthlyAmount).toHaveTextContent('$100');
    fireEvent.click(increaseMonthNavigator);
    fireEvent.click(increaseMonthNavigator);
    expect(monthlyAmount).toHaveTextContent('$33');
  });

  describe('tab navigation', () => {
    it('total amount input should have autofocus', () => {
      render(<BuyAHouseForm onSubmit={jest.fn()} />);
      expect(screen.getByTestId('amountInput')).toHaveFocus();
    });

    it('month picker should be focused after the tab key is pressed once', () => {
      render(<BuyAHouseForm onSubmit={jest.fn()} />);
      userEvent.tab();
      expect(screen.getByTestId('monthSelectorContainer')).toHaveFocus();
    });

    it('submit button should be focused after the tab key is pressed twice', () => {
      render(<BuyAHouseForm onSubmit={jest.fn()} />);
      userEvent.tab();
      userEvent.tab();
      expect(screen.getByTestId('buyAHouseSubmitButton')).toHaveFocus();
    });
  });
});
