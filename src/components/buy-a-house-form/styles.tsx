import styled from 'styled-components';
import { buttonStylesReset, inputsBorder } from '../../shared/mixins';

const actionButtonsFooter = styled('footer')`
  display: flex;
  justify-content: center;
  margin-top: 2rem;
`;

const submitButton = styled('button')`
  ${buttonStylesReset};
  font-family: inherit;
  font-weight: 600;
  background-color: ${(props) => props.theme.brandColorPrimary};
  color: ${(props) => props.theme.neutralWhite};
  width: min(100%, 320px);
  padding-top: 1rem;
  padding-bottom: 1rem;
  border-radius: 32px;

  &:hover {
    background: ${(props) => props.theme.brandColorSecondary};
    box-shadow: 0px 0px 0px transparent;
    text-shadow: 0px 0px 0px transparent;
    cursor: pointer;
  }
`;

const container = styled('form')`
  box-shadow: 0px 16px 32px rgba(30, 42, 50, 0.08);
  border-radius: 8px;
  font-size: 1.25rem;
  max-width: 560px;
  background-color: ${(props) => props.theme.neutralWhite};
  padding: 35px 40px 40px;
  margin: 0 auto;

  h2 {
    font-family: ${(props) => props.theme.fonts.secondary};
    font-weight: 500;
  }

  p {
    font-size: 1rem;
    color: ${(props) => props.theme.blueGray400};
  }

  header {
    display: flex;
    gap: 1rem;
    margin-bottom: 1.5rem;

    .texts {
      display: flex;
      flex-direction: column;
      flex: 1;
    }
  }

  .inputs {
    display: flex;
    flex-wrap: wrap;
    gap: 1rem;
    margin-bottom: 1.5rem;

    @media screen and (max-width: 378px) {
      flex-direction: column;
    }

    .input {
      display: inline-flex;
      flex-direction: column;
      flex: 1;

      .value {
        height: 3.5rem;
        ${inputsBorder};

        &.amount {
          display: flex;
          align-items: center;

          &:focus-within {
            outline: 2px ${(props) => props.theme.brandColorSecondary} solid;
          }

          .prefix-icon {
            margin: 0 0.75rem;
            display: inline-block;
          }

          & > input {
            font-family: ${(props) => props.theme.fonts.secondary};
            font-size: 1.5rem;
            color: ${(props) => props.theme.blueGray600};
            flex: 1;
            height: 100%;
            border: 0;
            width: 0;

            &:focus {
              outline: 0;
            }
          }
        }
      }

      &.total {
        flex: 1.5;
      }

      & > span {
        font-size: 0.75rem;
        line-height: 1.125rem;
        display: inline-block;
      }
    }
  }

  @media only screen and (max-width: 378px) {
    padding-right: 1.5rem;
    padding-left: 1.5rem;
  }
`;

const outputContainer = styled('section')`
  border-radius: 8px;
  border: 1px solid ${(props) => props.theme.blueGray50};
  background-color: ${(props) => props.theme.neutralWhite};
  margin-bottom: 1rem;
  .top {
    display: flex;
    justify-content: space-between;
    align-items: center;
    padding: 1rem 2rem;
    font-size: 1.5rem;

    .amount {
      color: ${(props) => props.theme.brandColorSecondary};
      font-weight: 500;
      font-family: ${(props) => props.theme.fonts.secondary};
      font-size: 2rem;
    }
  }

  .bottom {
    background-color: ${(props) => props.theme.blueGray10};
    padding: 1.5rem 2rem;
    font-size: 0.75rem;
  }

  @media only screen and (max-width: 378px) {
    .top {
      font-size: 1.125rem;

      .amount {
        font-size: 1.5rem;
      }
    }
    .bottom {
      text-align: center;
    }
  }
`;

export {
  outputContainer as OutputContainer,
  container as Container,
  submitButton as SubmitButton,
  actionButtonsFooter as ActionButtonsFooter,
};
