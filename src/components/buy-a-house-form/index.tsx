import React, { useEffect, useState } from 'react';
import CurrencyInput from 'react-currency-input';
import { ReactComponent as BuyAHouseIcon } from '../../assets/icons/buy-a-house.svg';
import { ReactComponent as DollarSign } from '../../assets/icons/dollar-sign.svg';
import { offsetDateInMonths, parseLocaleNumber } from '../../shared/utils';
import MonthSelector from '../month-selector';
import {
  ActionButtonsFooter,
  Container,
  OutputContainer,
  SubmitButton,
} from './styles';

const INITIAL_AMOUNT = 0;
const INITIAL_MONTHS_OFFSET = 1;

export default function BuyAHouseForm(props: {
  onSubmit: (e: React.FormEvent<HTMLFormElement>) => void;
}): JSX.Element {
  const [amount, setAmount] = useState(INITIAL_AMOUNT);
  const [formattedAmount, setFormattedAmount] = useState('--');
  const [monthlyAmountGoal, setMonthlyAmountGoal] = useState<number>(0);
  // Keeping track of an offset is simpler than calculating the difference
  // in months between two dates everytime something changes
  const [monthsOffset, setMonthsOffset] = useState<number>(
    INITIAL_MONTHS_OFFSET
  );
  const CURRENT_MONTH = new Date(
    new Date().getFullYear(),
    new Date().getMonth(),
    1
  );
  const MIN_DATE = new Date(
    CURRENT_MONTH.getFullYear(),
    CURRENT_MONTH.getMonth() + INITIAL_MONTHS_OFFSET,
    CURRENT_MONTH.getDate()
  );

  const [deadLine, setDeadLine] = useState<Readonly<Date>>(
    offsetDateInMonths(CURRENT_MONTH, INITIAL_MONTHS_OFFSET)
  );

  const increaseDeadlineOffset = () => setMonthsOffset(monthsOffset + 1);
  const decreaseDeadlineOffset = () => setMonthsOffset(monthsOffset - 1);

  const decreaseDeadlineOffsetIfPossible = () => {
    if (monthsOffset > INITIAL_MONTHS_OFFSET) {
      decreaseDeadlineOffset();
    }
  };

  useEffect(() => {
    setFormattedAmount(amountFormatter.format(amount));
    setMonthlyAmountGoal(amount / monthsOffset);
  }, [amount, monthsOffset]);

  return (
    <Container data-testid="buyAHouseFormContainer" onSubmit={props.onSubmit}>
      <header>
        <BuyAHouseIcon role="presentation" />
        <div className="texts">
          <h2>Buy a house</h2>
          <p>Saving goal</p>
        </div>
      </header>
      <section className="inputs">
        <label className="input total">
          <span>Total amount</span>
          <div className="value amount">
            <DollarSign className="prefix-icon" />
            <CurrencyInput
              precision="0"
              inputMode="numeric"
              name="amount"
              value={amount}
              onChangeEvent={(e: React.ChangeEvent<HTMLInputElement>) => {
                const newAmount = parseLocaleNumber(e.target.value, 'en-US');
                setAmount(newAmount);
              }}
              data-testid="amountInput"
              autoFocus
            />
          </div>
        </label>
        <div className="input">
          <span aria-hidden>Reach goal by </span>
          <MonthSelector
            className="value"
            min={MIN_DATE}
            onChange={(newDeadline) => setDeadLine(newDeadline)}
            date={deadLine}
            onIncrease={increaseDeadlineOffset}
            onDecrease={decreaseDeadlineOffsetIfPossible}
          />
        </div>
      </section>
      <OutputContainer>
        <div className="top">
          <span>Monthly amount </span>
          <span data-testid="monthlyAmount" className="amount">
            {amountFormatter
              .format(monthlyAmountGoal)
              .replace(/\D00(?=\D*$)/, '')}
          </span>
        </div>
        <div className="bottom">
          You&apos;re planning{' '}
          <strong data-testid="monthlyDeposits">
            {monthsOffset} monthly deposit{monthsOffset > 1 ? 's' : ''}
          </strong>{' '}
          to reach your{' '}
          <strong data-testid="formattedTotalAmount">{formattedAmount}</strong>{' '}
          goal by{' '}
          <strong data-testid="deadline">
            {dateFormatter.format(deadLine as Date)}
          </strong>
          .
        </div>
      </OutputContainer>
      <ActionButtonsFooter>
        <SubmitButton type="submit" data-testid="buyAHouseSubmitButton">
          Confirm
        </SubmitButton>
      </ActionButtonsFooter>
    </Container>
  );
}

const amountFormatter = Intl.NumberFormat('en-US', {
  style: 'currency',
  currency: 'USD',
  maximumFractionDigits: 0,
  minimumFractionDigits: 0,
});

const dateFormatter = Intl.DateTimeFormat('en-US', {
  year: 'numeric',
  month: 'long',
});
