# Architectural decision records

- Three components were created:
    - **Scaffold**: the visual application scaffold, which contains the header and main components;
    - **Month-selector**: a month-picking component that follows the requirements given in the Development Instructions and fits accessibility criteria, including screen-readers support, tab-navigation and adequate visual outline when focused;
    - **Buy A House Form**: the form composed by the two input components and an output section, ending with a Submit button. This component concentrates the business logic for calculating the monthly deposits.
- The main point of concern was dealing with accessibilty: 
    - Visually-impaired users needed to be able to use the month-selector with no issues. This was achieved by using ARIA attributes. Screen readers will read the picked month outloud as soon as the user presses the left or right arrow keys when the picker is focused. 
    - Decorative images were given the [`role="presentation"`](https://www.w3.org/WAI/tutorials/images/decorative/) attribute so screen-readers would pass them by.
    - The *Goal Amount* input was given the [`inputmode="numeric"`](https://developer.mozilla.org/en-US/docs/Web/HTML/Global_attributes/inputmode) attribute so the virtual keyboard on mobile devices display a numeric keyboard upon focus.
- Automated test focused on two things: 
    - testing the *Month-Selector* component, its rules and its accessibility features
    - testing the *Buy A House Form* component, its business logic, formatting, and accessibility features (e.g. tab navigation)
- The [Intl browser API](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Intl) was used in order to format some Date and Currency values.
- It was opted to only add one external library apart from the ones initially added: `react-currency-input`. The overhead imposed by installing any major components lib would be unlikely worthy for a project of this size.
- All styling was done by hand using styled-components and modern CSS.
- At first sight, elements were going to be arranged with CSS grid, but flex turned out to be a good fit.

## Development Instructions

### Evaluation
Be aware that Origin will mainly take into consideration the following evaluation criteria:
* How close your page is to the mockups, both on mobile & desktop;
* How clean and organized your code is;
* How good your automated tests are, i.e.: qualitative over quantitative (in case of usage of this base project, feel free to choose between jest or testing library);
* If you implemented the business rules correctly.

#### Money input

The money input component should:

- Allow only numbers
- Display the value formatted as money (e.g 3500.45 should be 3,500.44)
- We recommend you name this input as "amount"

#### Date input

The date input component should:

- Allow only future months
- When clicking on the arrow buttons it should go up and down month by month
- On focused, the users should be able to move the months by typing the Left and Right arrow key on the keyboard
- We recommend you name this input as "reachDate"

#### Confirm button

You don't need to add any action on the confirmation button

# Usage

This project requires the latest LTS version of NodeJS and you may need to install the yarn as global dependency
```bash
npm install -g yarn
```

After you have cloned this repo and install the yarn, install the dependencies with:

```
yarn install
```

You can then start the application running:

```
yarn start
```

That's it. Just Access `http://localhost:3000` in your browser.

### Linting and Format

```
yarn lint
yarn format
```

### Testing

```
yarn test
```
